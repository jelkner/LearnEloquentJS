let speak = function(line) {
    document.write(`The ${this.type} rabbit says '${line}'\n`);
};
let whiteRabbit = {type: "white", speak};
let hungryRabbit = {type: "hungry", speak};

whiteRabbit.speak("Oh my ears and whiskers, how late it's getting!");
hungryRabbit.speak("I could use a carrot about now.");

function normalize() {
    return this.coords.map(n => n / this.length);
}
document.write(normalize.call({coords: [1, 14, 7, 20, 28], length: 7}));
document.write('\n');

/*
function Rabbit(type) {
    this.type = type;
}
Rabbit.prototype.speak = function(line) {
    document.write(`The ${this.type} rabbit says '${line}'\n`);
};
*/
// New way to write above:
class Rabbit {
    constructor(type) {
        this.type = type;
    }
    speak(line) {
        document.write(`The ${this.type} rabbit says '${line}'\n`);
    }
}

let weirdRabbit = new Rabbit("weird");
weirdRabbit.speak("Wamp, snarkertumpt, blark!");

/*
Note the distinction between the prototype constructor (a function) and
the object's prototype (refers to instances created through the prototype
constructor.
 */
document.write("Prototype of Rabbit: " + Object.getPrototypeOf(Rabbit) + "\n");
document.write(
    "Prototype of weirdRabbit: " + Object.getPrototypeOf(weirdRabbit) + "\n"
);

let myMatrix = new Matrix(3, 2, (x, y) => `value ${x + y}`);
for (let elem of myMatrix) {
    document.write(elem.value, '\n');
}
