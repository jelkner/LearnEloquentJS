/*
First version as descripted in exersize

function flatten(a) {
    return a.reduce((a, b) => a.concat(b));
}
*/
// Extended version supporting arbitary nesting of arrays
function flatten(original, flattened = []) {
    for (let elem of original) {
        if (Array.isArray(elem)) {
            flattened.concat(flatten(elem, flattened));
        } else {
            flattened.push(elem);
        }
    }
    return flattened; 
}

function loop(val, test, update, body) {
    while (test(val)) {
        body(val);
        val = update(val);
    }
    return val;
}

function every(a, test) {
    for (let elem of a) {
        if (!test(elem)) {
            return false;
        }
    }
    return true;
}

function every2(a, test) {
    return !a.some(x => !test(x));
}

function characterScript(code) {
    for (let script of SCRIPTS) {
        if (script.ranges.some(([from, to]) => {
            return code >= from && code < to;
        })) {
            return script;
        }
    }
    return null;
}

function dominantDirection(text) {
    let directionCounts = {};
    for (let ch of text) {
        let chscript = characterScript(ch.charCodeAt(0));
        // Just skip over characters not in a character set
        if (chscript == null) continue;
        direction = chscript.direction;
        if (directionCounts.hasOwnProperty(direction)) {
            directionCounts[direction]++;
        } else {
            directionCounts[direction] = 1;
        }
    }
    let direction_counts_list = Object.entries(directionCounts);
    let maxdir = direction_counts_list[0];
    for (let pair of direction_counts_list) {
        if (pair[1] > maxdir[1]) maxdir = pair; 
    }
    return maxdir[0];
}
