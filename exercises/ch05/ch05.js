function repeat(n, action) {
    for (let i = 0; i < n; i++) {
        action(i);
    }
}
repeat(5, i => {document.write(`This is repeation number ${i + 1}.\n`);});

function filter(array, test) {
    let passed = [];
    for (let element of array) {
        if (test(element)) {
            passed.push(element);
        }
    }
    return passed;
}

/* living = filter(SCRIPTS, script => script.living);
for (let script of living) {
    document.write(script.name);
    document.write("\n");
}
*/
let myFunc = s => document.write(s.name + '\n');
// SCRIPTS.filter(s => s.living).forEach(myFunc);
