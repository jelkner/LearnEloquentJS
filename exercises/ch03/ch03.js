const hummus = function(factor) {
    if (isNaN(factor)) factor = 1;
    const ingredient = function(amount, unit, name) {
        let ingredientAmount = amount * factor;
        if (ingredientAmount > 1) {
            unit += "s";
        }
        document.write(`${ingredientAmount} ${unit} ${name}\n`);
    };
    ingredient(1, "can", "chickpeas");
    ingredient(0.25, "cup", "tahini");
    ingredient(0.25, "cup", "lemon juice");
    ingredient(1, "clove", "garlic");
    ingredient(2, "tablespoon", "olive oil");
    ingredient(0.5, "teaspoon", "cumin");
};

let myFoo = function() {
    document.write('myFoo called with ' + arguments.length + ' arguments.\n');
};

/*
What follows is a long version of what the text presents as:
function multiplier(factor) {
    return number => number * factor;
}
*/
function multiplier(factor) {
    function foo(n) {
        return n * factor;
    };
    return foo;
}

let thrice = multiplier(3);
let fourit = multiplier(4);


myFoo();
myFoo(4, 'cheese');
myFoo([1, 2, 3], 'batman', 4, 'cheese');
hummus();
document.write("thrice 5 is " + thrice(5) + "\n");
document.write("fourit 5 is " + fourit(5) + "\n");

document.write("\nChapter 3, Exercise 1:\n");
function min(n, m) {
    return n > m ? m : n;
};
document.write("min(0, 10) is " + min(0, 10) + "\n");
document.write("min(0, -10) is " + min(0, -10) + "\n");

document.write("\nChapter 3, Exercise 2:\n");
let isEven = function(n) {
    if (n < 0) return isEven(-n);
    if (n === 0) return true;
    if (n === 1) return false;
    return isEven(n - 2);
};
document.write("isEven(50)? " + isEven(50) + "\n");
document.write("isEven(75)? " + isEven(75) + "\n");
document.write("isEven(-1)? " + isEven(-1) + "\n");

document.write("\nChapter 3, Exercise 3:\n");
let countChar = function(s, ch) {
    let count = 0;
    for (let i = 0; i < s.length; i++) {
        if (s[i] === ch) count++;
    };
    return count;
};
let countBs = s => countChar(s, 'B');
document.write("There are " + countBs("BBC") + " Bs in 'BBC'\n");
