let listOfNumbers = [2, 3, 5, 7, 11];
document.write(listOfNumbers + '\n');

// Experiment with push
/*
document.write("\npush experiment:\n");
let myStack = [];
let val;

while (true) {
    val = prompt("Enter a push value or 'stop!' to stop: ");
    if (val === 'stop!') break;
    myStack.push(val);
}
document.write("The last value added to myStack is " + myStack.pop() + "\n");
document.write("The 2nd to last value added is " + myStack.pop() + "\n");
document.write("The 3nd to last value added is " + myStack.pop() + "\n");
document.write("What's left in myStack is " + myStack + '\n');
*/

let objA = {
    prop1: 42,
    prop2: 'Silly Rabit',
    prop3: [2, 4, 8, 16, 32],
    prop4: {
        p1: 'p1 str',
        p2: 19
    }
};
document.write(Object.keys(objA) + '\n');

// An experiment with object creation using existing names.
let n = 97;
let name = "Bob";
let truth = true;
let list = [3, 6, 9, 12];
let newObj = {n, name, truth, list};
document.write(Object.keys(newObj) + '\n');
document.write(newObj.list + '\n');

let journal = export_journal();
document.write(journal.length + '\n');

function phi([n00, n01, n10, n11]) {
    return (n11 * n00 - n10 * n01) /
            Math.sqrt((n10 + n11) * (n00 + n01) *
                      (n01 + n11) * (n00 + n10)
            );
}

function tableFor(event, journal) {
    let table = [0, 0, 0, 0];
    for (let entry of journal) {
        let index = 0;
        if (entry.events.includes(event)) index += 1;
        if (entry.squirrel) index += 2;
        table[index] += 1;
    }
    return table;
}

function journalEvents(journal) {
    let events = [];
    for (let entry of journal) {
        for (let event of entry.events) {
            if (!events.includes(event)) {
                events.push(event);
            }
        }
    }
    return events;
}

document.write(phi([76, 9, 4, 1]) + '\n');
document.write(tableFor("pizza", journal) + '\n');
document.write(journalEvents(journal) + '\n');

for (let event of journalEvents(journal)) {
    document.write(event + ":", phi(tableFor(event, journal)) + '\n');
}

document.write("\nChapter 4, Exercise 1:\n");
function range(start, end) {
    nums = [];
    for (let i = start; i <= end; i++) {
        nums.push(i);
    }
    return nums;
}

let sum = function(nums) {
    result = 0;
    for (let num of nums) {
        result += num;
    }
    return result;
};
document.write("range(3, 7) is " + range(3, 7) + '\n');
document.write("sum(range(1, 10) is " + sum(range(1, 10)) + '\n');

document.write("\nChapter 4, Exercise 2:\n");
function reverseArray(a) {
    let newArray = [];
    for (let elem of a) {
        newArray.unshift(elem);
    }
    return newArray;
}
function reverseArrayInPlace(a) {
    let start = 0;
    let end = a.length - 1;
    while (start < end) {
        temp = a[start];
        a[start++] = a[end];
        a[end--] = temp;
    }
}
let arrayVals = [1, 2, 3, 4, 5];
document.write("reverseArray(arrayVals) is " + reverseArray(arrayVals) + '\n');
reverseArrayInPlace(arrayVals);
document.write("arrayVals after reverseArrayInPlace is " + arrayVals + '\n');

document.write("\nChapter 4, Exercise 3:\n");
function arrayToList(a) {
    let next = {value: undefined, rest: {}};
    let list = next, current;
    for (let val of a) {
        current = next;
        current.value = val;
        current.rest = {};
        next = next.rest;
    }
    return list;
}
function listToArray(list) {
    let a = [];
    while (list.rest) {
        a.push(list.value);
        list = list.rest;
    }
    return a;
}
list = arrayToList([1, 3, 7, 11, 93]); 
document.write("arrayToList([1, 3, 7, 11, 93): ");
document.write(JSON.stringify(list, null, 4) + "\n");
document.write("listToArray(list): " + listToArray(list) + "\n");
