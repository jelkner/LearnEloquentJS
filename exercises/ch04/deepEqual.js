function deepEqual(thing1, thing2) {
    // If two things have different types, they trivially not equal
    if (typeof thing1 !== typeof thing2) {
        return false;
    }
    // Now handle primitive types
    if (Object(thing1) !== thing1)  // things are a primitive type
        return (thing1 === thing2);
    // Handle arrays
    if (Array.isArray(thing1)) {
        // They're not equal if they don't have the same length
        if (thing1.length !== thing2.length) {
            return false;
        }
        // If they have the same length, deepEqual compare their values
        for (let i = 0; i < thing1.length; i++) {
            if (!deepEqual(thing1[i], thing2[i])) {
                return false;
            }
        }
    }
    // Handle objects by recursely comparing their property:value pairs
    for (property of Object.keys(thing1)) {
        if (!deepEqual(thing1[property], thing2[property])) {
            return false;
        }
    }
    // If a difference between thing1 and thing2 isn't found, they are equal
    return true;
}
