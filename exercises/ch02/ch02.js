function factorial(n) {
    if (n == 0) {
        return 1;
    } else {
        return factorial(n - 1)  * n;
    }
}
document.write("The result is: " + factorial(8) + ".\n");
document.write(`Half of 120 is ${120/2}.\n`);

let ten = 10;

document.write(`10 times 10 is ${ten * ten}.\n\n`);

// let x = Number(prompt("So, just what value do you think x should have? "));
// document.write("The value of x is " + x + ".");

// Print a triangle of #s
document.write("\nChapter 2, Exercise 1:\n");
let s = '#'
for (let i = 1; i < 8; i++) {
    document.write(s.repeat(i) + '\n');
}

document.write("\nChapter 2, Exercise 2:\n");
for (i = 1; i < 101; i++) {
    if (i % 3 === 0 && i % 5 === 0)
        document.write("FizzBuzz")
    else if (i % 3 === 0)
        document.write("Fizz")
    else if (i % 5 === 0)
        document.write("Buzz")
    else
        document.write(i);
    document.write('\n');
}

document.write("\nChapter 2, Exercise 3:\n");
let board = '';
let ch = '#';
let size = 8;

for (row = 0; row < size; row++) {
    for (col = 0; col < size; col++) {
        ch = (ch === '#' ? ' ' : '#'); 
        board += ch;
    }
    board += '\n';
    if (col % 2 === 0)
        ch = (ch === '#' ? ' ' : '#'); 
}
document.write(board);
