function factorial(n) {
    if (n == 0) {
        return 1;
    } else {
        return factorial(n - 1)  * n;
    }
}
console.log("The result is: " + factorial(8) + ".\n");
console.log(`Half of 120 is ${120/2}.\n`);

let ten = 10;

console.log(`10 times 10 is ${ten * ten}.\n\n`);

// let x = Number(prompt("So, just what value do you think x should have? "));
// console.log("The value of x is " + x + ".");

// Print a triangle of #s
console.log("\nChapter 2, Exercise 1:\n");
let s = '#'
for (let i = 1; i < 8; i++) {
    console.log(s.repeat(i) + '\n');
}

console.log("\nChapter 2, Exercise 2:\n");
for (i = 1; i < 101; i++) {
    if (i % 3 === 0 && i % 5 === 0)
        console.log("FizzBuzz")
    else if (i % 3 === 0)
        console.log("Fizz")
    else if (i % 5 === 0)
        console.log("Buzz")
    else
        console.log(i);
    console.log('\n');
}

console.log("\nChapter 2, Exercise 3:\n");
let board = '';
let ch = '#';
let size = 8;

for (row = 0; row < size; row++) {
    for (col = 0; col < size; col++) {
        ch = (ch === '#' ? ' ' : '#'); 
        board += ch;
    }
    board += '\n';
    if (col % 2 === 0)
        ch = (ch === '#' ? ' ' : '#'); 
}
console.log(board);
