# Testing JavaScript

[TDD](https://en.wikipedia.org/wiki/Test-driven_development) and its
descendant,
[Behavior-driven development](https://en.wikipedia.org/wiki/Behavior-driven_development)
 (BDD), offer us software development practices that lead to better software by
helping us think about what we aim to do *before* we try to do it.

As a programming teacher for almost 30 years, I seen so many struggling
aspiring programmers trying to master the craft by <q>throwing code</q> at
a problem almost willy-nilly, and then coming to me to ask for help when the
resultant mess doesn't behave as they hoped it would. 

During each of these occasions I find myself wishing I could help students
learn a more careful, step-by-step process that would help them experience
more success.

**TDD** and **BDD** offer just such an approach, and this directory will be
dedicated to our experiments with finding an easy way for beginners to
follow these practices while learning JavaScript.
